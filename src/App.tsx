import './App.css'
import { Route, Routes } from 'react-router-dom'
import Navegador from './components/Navegaddor'
import Home from './pages/home/Home'
import { Flex } from "@chakra-ui/react"
import Cadastro from './pages/cadastro/Cadastro'
import Clientes from './pages/clientes/Clientes'
import Sobre from './pages/sobre/Sobre'

function App() {


  return (
    <Flex
      flexFlow={'column'}
      h="100vh"
    >
      <Navegador></Navegador>
      <Routes>
        <Route path='/' element={<Home></Home>}></Route>
        <Route path='/cadastro' element={<Cadastro></Cadastro>}></Route>
        <Route path='/clientes' element={<Clientes></Clientes>}></Route>
        <Route path='/sobre' element={<Sobre></Sobre>}></Route>
      </Routes>
    </Flex>

  )
}

export default App
