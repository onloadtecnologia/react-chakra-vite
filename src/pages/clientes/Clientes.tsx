import { Box, Flex, Center, Text } from "@chakra-ui/react";

export default function Clientes() {
    return (
        <Flex
        flexDirection="column"
        flexGrow={1}
        flexBasis="auto"
        justifyContent="center"
        alignItems="center"
        >

            <Center>
                <Text fontWeight={'bold'}>Clientes</Text>
            </Center>
        </Flex>
    )
}