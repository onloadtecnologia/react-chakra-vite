import { Button, Flex, FormControl, FormLabel, Input } from "@chakra-ui/react";
import { useState } from "react";

export default function Cadastro() {
    const [cliente, setCliente] = useState({
        nome: "",
        telefone: "",
        email: "",
        cep: ""
    })
    function onSubmit(){
        alert(JSON.stringify(cliente));
    }
    return (

        <FormControl w={'50%'} m={'auto'}>

            <Flex flexDirection={'column'} mb={3}>
                <FormLabel fontWeight={'bold'}>Nome</FormLabel>
                <Input type={'text'}
                    onChange={(nome) => setCliente({ ...cliente, nome: nome.target.value })}
                    borderRadius={5} borderColor={'blackAlpha.400'}
                >
                </Input>
            </Flex>

            <Flex flexDirection={'column'} mb={3}>
                <FormLabel fontWeight={'bold'}>Telefone</FormLabel>
                <Input type={'tel'}
                    onChange={(telefone) => setCliente({ ...cliente, telefone: telefone.target.value })}
                    borderRadius={5} borderColor={'blackAlpha.400'}>
                </Input>
            </Flex>

            <Flex flexDirection={'column'} mb={3}>
                <FormLabel fontWeight={'bold'}>Email</FormLabel>
                <Input type={'email'}
                    onChange={(email) => setCliente({ ...cliente, email: email.target.value })}
                    borderRadius={5} borderColor={'blackAlpha.400'}>
                </Input>
            </Flex>

            <Flex flexDirection={'column'} mb={3}>
                <FormLabel fontWeight={'bold'}>Cep</FormLabel>
                <Input type={'number'}
                    onChange={(cep) => setCliente({ ...cliente, cep: cep.target.value })}
                    borderRadius={5} borderColor={'blackAlpha.400'}>
                </Input>
            </Flex>

            <Flex justifyContent={'end'}>
                <Button type="submit"
                onClick={()=> onSubmit()}
                 colorScheme={'orange'} color={'blackAlpha'} leftIcon={<i className="material-icons">send</i>}>Salvar</Button>
            </Flex>

        </FormControl>
    )
}