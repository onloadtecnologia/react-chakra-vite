import { Center,Text, Flex } from "@chakra-ui/react";

export default function Sobre() {

    return(
        <Flex
        flexDirection={'column'}
        flexGrow={1}
        flexBasis={0}
        justifyContent={'center'}
        alignItems={'center'}
        >
        <Center>
            <Text fontWeight={'bold'}>Sobre</Text>
        </Center>
        </Flex>
    )
    
}