import { Center, Flex, Text } from '@chakra-ui/react'
import './index.css'
export default function Home() {
    return (
        <Flex
            flexDirection="column"
            flexGrow={1}
            flexBasis="auto"
            justifyContent="center"
            alignItems="center"
            bgGradient={[
                'linear(to-tr, teal.300, yellow.400)',
                'linear(to-t, blue.200, teal.500)',
                'linear(to-b, orange.100, purple.300)',
              ]}
        >
            <Center>
                <Text>Home</Text>
            </Center>
        </Flex>
    )

}

