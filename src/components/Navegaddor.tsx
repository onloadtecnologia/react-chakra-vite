import { Box, Flex, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import './index.css'

function Navegador() {

    return (
        <Flex
            flexGrow={0}
            flexBasis="auto"
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
            h={70}
            p={5}
            color="white"
            bg="black"
        >

            <Text me={3}>
                <Link to={"/"}>
                    <a>Home</a>
                </Link>
            </Text>

            <Text me={3}>
                <Link to={"/cadastro"}>
                    <a>Cadastro</a>
                </Link>
            </Text>

            <Text me={3}>
                <Link to={"/clientes"}>
                    <a>Clientes</a>
                </Link>
            </Text>

            <Text me={3}>
                <Link to={"/sobre"}>
                    <a>Sobre</a>
                </Link>
            </Text>

        </Flex>
    )

}

export default Navegador;