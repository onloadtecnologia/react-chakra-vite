export default class Cliente {
    id: number = 0;
    data: Date = new Date();
    nome: String = "";
    telefone: String = "";
    email: String = "";
    cep: String = "";
}
